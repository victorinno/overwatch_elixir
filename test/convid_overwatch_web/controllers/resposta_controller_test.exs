defmodule ConvidOverwatchWeb.RespostaControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch

  @create_attrs %{resposta: "some resposta"}
  @update_attrs %{resposta: "some updated resposta"}
  @invalid_attrs %{resposta: nil}

  def fixture(:resposta) do
    {:ok, resposta} = Overwatch.create_resposta(@create_attrs)
    resposta
  end

  describe "index" do
    test "lists all respostas", %{conn: conn} do
      conn = get(conn, Routes.resposta_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Respostas"
    end
  end

  describe "new resposta" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.resposta_path(conn, :new))
      assert html_response(conn, 200) =~ "New Resposta"
    end
  end

  describe "create resposta" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.resposta_path(conn, :create), resposta: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.resposta_path(conn, :show, id)

      conn = get(conn, Routes.resposta_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Resposta"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.resposta_path(conn, :create), resposta: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Resposta"
    end
  end

  describe "edit resposta" do
    setup [:create_resposta]

    test "renders form for editing chosen resposta", %{conn: conn, resposta: resposta} do
      conn = get(conn, Routes.resposta_path(conn, :edit, resposta))
      assert html_response(conn, 200) =~ "Edit Resposta"
    end
  end

  describe "update resposta" do
    setup [:create_resposta]

    test "redirects when data is valid", %{conn: conn, resposta: resposta} do
      conn = put(conn, Routes.resposta_path(conn, :update, resposta), resposta: @update_attrs)
      assert redirected_to(conn) == Routes.resposta_path(conn, :show, resposta)

      conn = get(conn, Routes.resposta_path(conn, :show, resposta))
      assert html_response(conn, 200) =~ "some updated resposta"
    end

    test "renders errors when data is invalid", %{conn: conn, resposta: resposta} do
      conn = put(conn, Routes.resposta_path(conn, :update, resposta), resposta: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Resposta"
    end
  end

  describe "delete resposta" do
    setup [:create_resposta]

    test "deletes chosen resposta", %{conn: conn, resposta: resposta} do
      conn = delete(conn, Routes.resposta_path(conn, :delete, resposta))
      assert redirected_to(conn) == Routes.resposta_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.resposta_path(conn, :show, resposta))
      end
    end
  end

  defp create_resposta(_) do
    resposta = fixture(:resposta)
    {:ok, resposta: resposta}
  end
end
