defmodule ConvidOverwatchWeb.PertenceControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch

  @create_attrs %{}
  @update_attrs %{}
  @invalid_attrs %{}

  def fixture(:pertence) do
    {:ok, pertence} = Overwatch.create_pertence(@create_attrs)
    pertence
  end

  describe "index" do
    test "lists all pertencimentos", %{conn: conn} do
      conn = get(conn, Routes.pertence_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Pertencimentos"
    end
  end

  describe "new pertence" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.pertence_path(conn, :new))
      assert html_response(conn, 200) =~ "New Pertence"
    end
  end

  describe "create pertence" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.pertence_path(conn, :create), pertence: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.pertence_path(conn, :show, id)

      conn = get(conn, Routes.pertence_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Pertence"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.pertence_path(conn, :create), pertence: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Pertence"
    end
  end

  describe "edit pertence" do
    setup [:create_pertence]

    test "renders form for editing chosen pertence", %{conn: conn, pertence: pertence} do
      conn = get(conn, Routes.pertence_path(conn, :edit, pertence))
      assert html_response(conn, 200) =~ "Edit Pertence"
    end
  end

  describe "update pertence" do
    setup [:create_pertence]

    test "redirects when data is valid", %{conn: conn, pertence: pertence} do
      conn = put(conn, Routes.pertence_path(conn, :update, pertence), pertence: @update_attrs)
      assert redirected_to(conn) == Routes.pertence_path(conn, :show, pertence)

      conn = get(conn, Routes.pertence_path(conn, :show, pertence))
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, pertence: pertence} do
      conn = put(conn, Routes.pertence_path(conn, :update, pertence), pertence: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Pertence"
    end
  end

  describe "delete pertence" do
    setup [:create_pertence]

    test "deletes chosen pertence", %{conn: conn, pertence: pertence} do
      conn = delete(conn, Routes.pertence_path(conn, :delete, pertence))
      assert redirected_to(conn) == Routes.pertence_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.pertence_path(conn, :show, pertence))
      end
    end
  end

  defp create_pertence(_) do
    pertence = fixture(:pertence)
    {:ok, pertence: pertence}
  end
end
