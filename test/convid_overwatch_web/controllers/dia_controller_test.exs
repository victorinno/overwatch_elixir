defmodule ConvidOverwatchWeb.DiaControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch

  @create_attrs %{dia: ~D[2010-04-17]}
  @update_attrs %{dia: ~D[2011-05-18]}
  @invalid_attrs %{dia: nil}

  def fixture(:dia) do
    {:ok, dia} = Overwatch.create_dia(@create_attrs)
    dia
  end

  describe "index" do
    test "lists all dias", %{conn: conn} do
      conn = get(conn, Routes.dia_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Dias"
    end
  end

  describe "new dia" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.dia_path(conn, :new))
      assert html_response(conn, 200) =~ "New Dia"
    end
  end

  describe "create dia" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.dia_path(conn, :create), dia: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.dia_path(conn, :show, id)

      conn = get(conn, Routes.dia_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Dia"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.dia_path(conn, :create), dia: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Dia"
    end
  end

  describe "edit dia" do
    setup [:create_dia]

    test "renders form for editing chosen dia", %{conn: conn, dia: dia} do
      conn = get(conn, Routes.dia_path(conn, :edit, dia))
      assert html_response(conn, 200) =~ "Edit Dia"
    end
  end

  describe "update dia" do
    setup [:create_dia]

    test "redirects when data is valid", %{conn: conn, dia: dia} do
      conn = put(conn, Routes.dia_path(conn, :update, dia), dia: @update_attrs)
      assert redirected_to(conn) == Routes.dia_path(conn, :show, dia)

      conn = get(conn, Routes.dia_path(conn, :show, dia))
      assert html_response(conn, 200)
    end

    test "renders errors when data is invalid", %{conn: conn, dia: dia} do
      conn = put(conn, Routes.dia_path(conn, :update, dia), dia: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Dia"
    end
  end

  describe "delete dia" do
    setup [:create_dia]

    test "deletes chosen dia", %{conn: conn, dia: dia} do
      conn = delete(conn, Routes.dia_path(conn, :delete, dia))
      assert redirected_to(conn) == Routes.dia_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.dia_path(conn, :show, dia))
      end
    end
  end

  defp create_dia(_) do
    dia = fixture(:dia)
    {:ok, dia: dia}
  end
end
