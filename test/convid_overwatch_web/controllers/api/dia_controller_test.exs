defmodule ConvidOverwatchWeb.Api.DiaControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Dia

  @create_attrs %{
    dia: ~D[2010-04-17]
  }
  @update_attrs %{
    dia: ~D[2011-05-18]
  }
  @invalid_attrs %{dia: nil}

  def fixture(:dia) do
    {:ok, dia} = Overwatch.create_dia(@create_attrs)
    dia
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all dias", %{conn: conn} do
      conn = get(conn, Routes.api_dia_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create dia" do
    test "renders dia when data is valid", %{conn: conn} do
      conn = post(conn, Routes.api_dia_path(conn, :create), dia: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.api_dia_path(conn, :show, id))

      assert %{
               "id" => id,
               "dia" => "2010-04-17"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.api_dia_path(conn, :create), dia: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update dia" do
    setup [:create_dia]

    test "renders dia when data is valid", %{conn: conn, dia: %Dia{id: id} = dia} do
      conn = put(conn, Routes.api_dia_path(conn, :update, dia), dia: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.api_dia_path(conn, :show, id))

      assert %{
               "id" => id,
               "dia" => "2011-05-18"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, dia: dia} do
      conn = put(conn, Routes.api_dia_path(conn, :update, dia), dia: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete dia" do
    setup [:create_dia]

    test "deletes chosen dia", %{conn: conn, dia: dia} do
      conn = delete(conn, Routes.api_dia_path(conn, :delete, dia))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.api_dia_path(conn, :show, dia))
      end
    end
  end

  defp create_dia(_) do
    dia = fixture(:dia)
    {:ok, dia: dia}
  end
end
