defmodule ConvidOverwatchWeb.Api.PerguntaControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Pergunta

  @create_attrs %{
    pergunta: "some pergunta"
  }
  @update_attrs %{
    pergunta: "some updated pergunta"
  }
  @invalid_attrs %{pergunta: nil}

  def fixture(:pergunta) do
    {:ok, pergunta} = Overwatch.create_pergunta(@create_attrs)
    pergunta
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all perguntas", %{conn: conn} do
      conn = get(conn, Routes.api_pergunta_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create pergunta" do
    test "renders pergunta when data is valid", %{conn: conn} do
      conn = post(conn, Routes.api_pergunta_path(conn, :create), pergunta: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.api_pergunta_path(conn, :show, id))

      assert %{
               "id" => id,
               "pergunta" => "some pergunta"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.api_pergunta_path(conn, :create), pergunta: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update pergunta" do
    setup [:create_pergunta]

    test "renders pergunta when data is valid", %{conn: conn, pergunta: %Pergunta{id: id} = pergunta} do
      conn = put(conn, Routes.api_pergunta_path(conn, :update, pergunta), pergunta: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.api_pergunta_path(conn, :show, id))

      assert %{
               "id" => id,
               "pergunta" => "some updated pergunta"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, pergunta: pergunta} do
      conn = put(conn, Routes.api_pergunta_path(conn, :update, pergunta), pergunta: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete pergunta" do
    setup [:create_pergunta]

    test "deletes chosen pergunta", %{conn: conn, pergunta: pergunta} do
      conn = delete(conn, Routes.api_pergunta_path(conn, :delete, pergunta))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.api_pergunta_path(conn, :show, pergunta))
      end
    end
  end

  defp create_pergunta(_) do
    pergunta = fixture(:pergunta)
    {:ok, pergunta: pergunta}
  end
end
