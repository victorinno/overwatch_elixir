defmodule ConvidOverwatchWeb.Api.RespostaControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Resposta

  @create_attrs %{
    resposta: "some resposta"
  }
  @update_attrs %{
    resposta: "some updated resposta"
  }
  @invalid_attrs %{resposta: nil}

  def fixture(:resposta) do
    {:ok, resposta} = Overwatch.create_resposta(@create_attrs)
    resposta
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all respostas", %{conn: conn} do
      conn = get(conn, Routes.api_resposta_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create resposta" do
    test "renders resposta when data is valid", %{conn: conn} do
      conn = post(conn, Routes.api_resposta_path(conn, :create), resposta: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.api_resposta_path(conn, :show, id))

      assert %{
               "id" => id,
               "resposta" => "some resposta"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.api_resposta_path(conn, :create), resposta: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update resposta" do
    setup [:create_resposta]

    test "renders resposta when data is valid", %{conn: conn, resposta: %Resposta{id: id} = resposta} do
      conn = put(conn, Routes.api_resposta_path(conn, :update, resposta), resposta: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.api_resposta_path(conn, :show, id))

      assert %{
               "id" => id,
               "resposta" => "some updated resposta"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, resposta: resposta} do
      conn = put(conn, Routes.api_resposta_path(conn, :update, resposta), resposta: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete resposta" do
    setup [:create_resposta]

    test "deletes chosen resposta", %{conn: conn, resposta: resposta} do
      conn = delete(conn, Routes.api_resposta_path(conn, :delete, resposta))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.api_resposta_path(conn, :show, resposta))
      end
    end
  end

  defp create_resposta(_) do
    resposta = fixture(:resposta)
    {:ok, resposta: resposta}
  end
end
