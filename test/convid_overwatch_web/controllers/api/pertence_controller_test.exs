defmodule ConvidOverwatchWeb.Api.PertenceControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Pertence

  @create_attrs %{

  }
  @update_attrs %{

  }
  @invalid_attrs %{}

  def fixture(:pertence) do
    {:ok, pertence} = Overwatch.create_pertence(@create_attrs)
    pertence
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all pertencimentos", %{conn: conn} do
      conn = get(conn, Routes.api_pertence_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create pertence" do
    test "renders pertence when data is valid", %{conn: conn} do
      conn = post(conn, Routes.api_pertence_path(conn, :create), pertence: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.api_pertence_path(conn, :show, id))

      assert %{
               "id" => id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.api_pertence_path(conn, :create), pertence: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update pertence" do
    setup [:create_pertence]

    test "renders pertence when data is valid", %{conn: conn, pertence: %Pertence{id: id} = pertence} do
      conn = put(conn, Routes.api_pertence_path(conn, :update, pertence), pertence: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.api_pertence_path(conn, :show, id))

      assert %{
               "id" => id
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, pertence: pertence} do
      conn = put(conn, Routes.api_pertence_path(conn, :update, pertence), pertence: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete pertence" do
    setup [:create_pertence]

    test "deletes chosen pertence", %{conn: conn, pertence: pertence} do
      conn = delete(conn, Routes.api_pertence_path(conn, :delete, pertence))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.api_pertence_path(conn, :show, pertence))
      end
    end
  end

  defp create_pertence(_) do
    pertence = fixture(:pertence)
    {:ok, pertence: pertence}
  end
end
