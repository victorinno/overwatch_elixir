defmodule ConvidOverwatchWeb.PerguntaControllerTest do
  use ConvidOverwatchWeb.ConnCase

  alias ConvidOverwatch.Overwatch

  @create_attrs %{pergunta: "some pergunta"}
  @update_attrs %{pergunta: "some updated pergunta"}
  @invalid_attrs %{pergunta: nil}

  def fixture(:pergunta) do
    {:ok, pergunta} = Overwatch.create_pergunta(@create_attrs)
    pergunta
  end

  describe "index" do
    test "lists all perguntas", %{conn: conn} do
      conn = get(conn, Routes.pergunta_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Perguntas"
    end
  end

  describe "new pergunta" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.pergunta_path(conn, :new))
      assert html_response(conn, 200) =~ "New Pergunta"
    end
  end

  describe "create pergunta" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.pergunta_path(conn, :create), pergunta: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.pergunta_path(conn, :show, id)

      conn = get(conn, Routes.pergunta_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Pergunta"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.pergunta_path(conn, :create), pergunta: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Pergunta"
    end
  end

  describe "edit pergunta" do
    setup [:create_pergunta]

    test "renders form for editing chosen pergunta", %{conn: conn, pergunta: pergunta} do
      conn = get(conn, Routes.pergunta_path(conn, :edit, pergunta))
      assert html_response(conn, 200) =~ "Edit Pergunta"
    end
  end

  describe "update pergunta" do
    setup [:create_pergunta]

    test "redirects when data is valid", %{conn: conn, pergunta: pergunta} do
      conn = put(conn, Routes.pergunta_path(conn, :update, pergunta), pergunta: @update_attrs)
      assert redirected_to(conn) == Routes.pergunta_path(conn, :show, pergunta)

      conn = get(conn, Routes.pergunta_path(conn, :show, pergunta))
      assert html_response(conn, 200) =~ "some updated pergunta"
    end

    test "renders errors when data is invalid", %{conn: conn, pergunta: pergunta} do
      conn = put(conn, Routes.pergunta_path(conn, :update, pergunta), pergunta: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Pergunta"
    end
  end

  describe "delete pergunta" do
    setup [:create_pergunta]

    test "deletes chosen pergunta", %{conn: conn, pergunta: pergunta} do
      conn = delete(conn, Routes.pergunta_path(conn, :delete, pergunta))
      assert redirected_to(conn) == Routes.pergunta_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.pergunta_path(conn, :show, pergunta))
      end
    end
  end

  defp create_pergunta(_) do
    pergunta = fixture(:pergunta)
    {:ok, pergunta: pergunta}
  end
end
