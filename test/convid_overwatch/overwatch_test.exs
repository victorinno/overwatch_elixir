defmodule ConvidOverwatch.OverwatchTest do
  use ConvidOverwatch.DataCase

  alias ConvidOverwatch.Overwatch

  describe "dias" do
    alias ConvidOverwatch.Overwatch.Dia

    @valid_attrs %{dia: ~D[2010-04-17]}
    @update_attrs %{dia: ~D[2011-05-18]}
    @invalid_attrs %{dia: nil}

    def dia_fixture(attrs \\ %{}) do
      {:ok, dia} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Overwatch.create_dia()

      dia
    end

    test "list_dias/0 returns all dias" do
      dia = dia_fixture()
      assert Overwatch.list_dias() == [dia]
    end

    test "get_dia!/1 returns the dia with given id" do
      dia = dia_fixture()
      assert Overwatch.get_dia!(dia.id) == dia
    end

    test "create_dia/1 with valid data creates a dia" do
      assert {:ok, %Dia{} = dia} = Overwatch.create_dia(@valid_attrs)
      assert dia.dia == ~D[2010-04-17]
    end

    test "create_dia/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Overwatch.create_dia(@invalid_attrs)
    end

    test "update_dia/2 with valid data updates the dia" do
      dia = dia_fixture()
      assert {:ok, %Dia{} = dia} = Overwatch.update_dia(dia, @update_attrs)
      assert dia.dia == ~D[2011-05-18]
    end

    test "update_dia/2 with invalid data returns error changeset" do
      dia = dia_fixture()
      assert {:error, %Ecto.Changeset{}} = Overwatch.update_dia(dia, @invalid_attrs)
      assert dia == Overwatch.get_dia!(dia.id)
    end

    test "delete_dia/1 deletes the dia" do
      dia = dia_fixture()
      assert {:ok, %Dia{}} = Overwatch.delete_dia(dia)
      assert_raise Ecto.NoResultsError, fn -> Overwatch.get_dia!(dia.id) end
    end

    test "change_dia/1 returns a dia changeset" do
      dia = dia_fixture()
      assert %Ecto.Changeset{} = Overwatch.change_dia(dia)
    end
  end

  describe "perguntas" do
    alias ConvidOverwatch.Overwatch.Pergunta

    @valid_attrs %{pergunta: "some pergunta"}
    @update_attrs %{pergunta: "some updated pergunta"}
    @invalid_attrs %{pergunta: nil}

    def pergunta_fixture(attrs \\ %{}) do
      {:ok, pergunta} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Overwatch.create_pergunta()

      pergunta
    end

    test "list_perguntas/0 returns all perguntas" do
      pergunta = pergunta_fixture()
      assert Overwatch.list_perguntas() == [pergunta]
    end

    test "get_pergunta!/1 returns the pergunta with given id" do
      pergunta = pergunta_fixture()
      assert Overwatch.get_pergunta!(pergunta.id) == pergunta
    end

    test "create_pergunta/1 with valid data creates a pergunta" do
      assert {:ok, %Pergunta{} = pergunta} = Overwatch.create_pergunta(@valid_attrs)
      assert pergunta.pergunta == "some pergunta"
    end

    test "create_pergunta/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Overwatch.create_pergunta(@invalid_attrs)
    end

    test "update_pergunta/2 with valid data updates the pergunta" do
      pergunta = pergunta_fixture()
      assert {:ok, %Pergunta{} = pergunta} = Overwatch.update_pergunta(pergunta, @update_attrs)
      assert pergunta.pergunta == "some updated pergunta"
    end

    test "update_pergunta/2 with invalid data returns error changeset" do
      pergunta = pergunta_fixture()
      assert {:error, %Ecto.Changeset{}} = Overwatch.update_pergunta(pergunta, @invalid_attrs)
      assert pergunta == Overwatch.get_pergunta!(pergunta.id)
    end

    test "delete_pergunta/1 deletes the pergunta" do
      pergunta = pergunta_fixture()
      assert {:ok, %Pergunta{}} = Overwatch.delete_pergunta(pergunta)
      assert_raise Ecto.NoResultsError, fn -> Overwatch.get_pergunta!(pergunta.id) end
    end

    test "change_pergunta/1 returns a pergunta changeset" do
      pergunta = pergunta_fixture()
      assert %Ecto.Changeset{} = Overwatch.change_pergunta(pergunta)
    end
  end

  describe "respostas" do
    alias ConvidOverwatch.Overwatch.Resposta

    @valid_attrs %{resposta: "some resposta"}
    @update_attrs %{resposta: "some updated resposta"}
    @invalid_attrs %{resposta: nil}

    def resposta_fixture(attrs \\ %{}) do
      {:ok, resposta} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Overwatch.create_resposta()

      resposta
    end

    test "list_respostas/0 returns all respostas" do
      resposta = resposta_fixture()
      assert Overwatch.list_respostas() == [resposta]
    end

    test "get_resposta!/1 returns the resposta with given id" do
      resposta = resposta_fixture()
      assert Overwatch.get_resposta!(resposta.id) == resposta
    end

    test "create_resposta/1 with valid data creates a resposta" do
      assert {:ok, %Resposta{} = resposta} = Overwatch.create_resposta(@valid_attrs)
      assert resposta.resposta == "some resposta"
    end

    test "create_resposta/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Overwatch.create_resposta(@invalid_attrs)
    end

    test "update_resposta/2 with valid data updates the resposta" do
      resposta = resposta_fixture()
      assert {:ok, %Resposta{} = resposta} = Overwatch.update_resposta(resposta, @update_attrs)
      assert resposta.resposta == "some updated resposta"
    end

    test "update_resposta/2 with invalid data returns error changeset" do
      resposta = resposta_fixture()
      assert {:error, %Ecto.Changeset{}} = Overwatch.update_resposta(resposta, @invalid_attrs)
      assert resposta == Overwatch.get_resposta!(resposta.id)
    end

    test "delete_resposta/1 deletes the resposta" do
      resposta = resposta_fixture()
      assert {:ok, %Resposta{}} = Overwatch.delete_resposta(resposta)
      assert_raise Ecto.NoResultsError, fn -> Overwatch.get_resposta!(resposta.id) end
    end

    test "change_resposta/1 returns a resposta changeset" do
      resposta = resposta_fixture()
      assert %Ecto.Changeset{} = Overwatch.change_resposta(resposta)
    end
  end

  describe "usuarios" do
    alias ConvidOverwatch.Overwatch.Usuario

    @valid_attrs %{email: "some email", nome: "some nome", senha_criptografada: "some senha_criptografada"}
    @update_attrs %{email: "some updated email", nome: "some updated nome", senha_criptografada: "some updated senha_criptografada"}
    @invalid_attrs %{email: nil, nome: nil, senha_criptografada: nil}

    def usuario_fixture(attrs \\ %{}) do
      {:ok, usuario} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Overwatch.create_usuario()

      usuario
    end

    test "list_usuarios/0 returns all usuarios" do
      usuario = usuario_fixture()
      assert Overwatch.list_usuarios() == [usuario]
    end

    test "get_usuario!/1 returns the usuario with given id" do
      usuario = usuario_fixture()
      assert Overwatch.get_usuario!(usuario.id) == usuario
    end

    test "create_usuario/1 with valid data creates a usuario" do
      assert {:ok, %Usuario{} = usuario} = Overwatch.create_usuario(@valid_attrs)
      assert usuario.email == "some email"
      assert usuario.nome == "some nome"
      assert usuario.senha_criptografada == "some senha_criptografada"
    end

    test "create_usuario/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Overwatch.create_usuario(@invalid_attrs)
    end

    test "update_usuario/2 with valid data updates the usuario" do
      usuario = usuario_fixture()
      assert {:ok, %Usuario{} = usuario} = Overwatch.update_usuario(usuario, @update_attrs)
      assert usuario.email == "some updated email"
      assert usuario.nome == "some updated nome"
      assert usuario.senha_criptografada == "some updated senha_criptografada"
    end

    test "update_usuario/2 with invalid data returns error changeset" do
      usuario = usuario_fixture()
      assert {:error, %Ecto.Changeset{}} = Overwatch.update_usuario(usuario, @invalid_attrs)
      assert usuario == Overwatch.get_usuario!(usuario.id)
    end

    test "delete_usuario/1 deletes the usuario" do
      usuario = usuario_fixture()
      assert {:ok, %Usuario{}} = Overwatch.delete_usuario(usuario)
      assert_raise Ecto.NoResultsError, fn -> Overwatch.get_usuario!(usuario.id) end
    end

    test "change_usuario/1 returns a usuario changeset" do
      usuario = usuario_fixture()
      assert %Ecto.Changeset{} = Overwatch.change_usuario(usuario)
    end
  end

  describe "pertencimentos" do
    alias ConvidOverwatch.Overwatch.Pertence

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def pertence_fixture(attrs \\ %{}) do
      {:ok, pertence} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Overwatch.create_pertence()

      pertence
    end

    test "list_pertencimentos/0 returns all pertencimentos" do
      pertence = pertence_fixture()
      assert Overwatch.list_pertencimentos() == [pertence]
    end

    test "get_pertence!/1 returns the pertence with given id" do
      pertence = pertence_fixture()
      assert Overwatch.get_pertence!(pertence.id) == pertence
    end

    test "create_pertence/1 with valid data creates a pertence" do
      assert {:ok, %Pertence{} = pertence} = Overwatch.create_pertence(@valid_attrs)
    end

    test "create_pertence/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Overwatch.create_pertence(@invalid_attrs)
    end

    test "update_pertence/2 with valid data updates the pertence" do
      pertence = pertence_fixture()
      assert {:ok, %Pertence{} = pertence} = Overwatch.update_pertence(pertence, @update_attrs)
    end

    test "update_pertence/2 with invalid data returns error changeset" do
      pertence = pertence_fixture()
      assert {:error, %Ecto.Changeset{}} = Overwatch.update_pertence(pertence, @invalid_attrs)
      assert pertence == Overwatch.get_pertence!(pertence.id)
    end

    test "delete_pertence/1 deletes the pertence" do
      pertence = pertence_fixture()
      assert {:ok, %Pertence{}} = Overwatch.delete_pertence(pertence)
      assert_raise Ecto.NoResultsError, fn -> Overwatch.get_pertence!(pertence.id) end
    end

    test "change_pertence/1 returns a pertence changeset" do
      pertence = pertence_fixture()
      assert %Ecto.Changeset{} = Overwatch.change_pertence(pertence)
    end
  end
end
