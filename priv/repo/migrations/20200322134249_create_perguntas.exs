defmodule ConvidOverwatch.Repo.Migrations.CreatePerguntas do
  use Ecto.Migration

  def change do
    create table(:perguntas) do
      add :pergunta, :string, size: 3000, null: false

      timestamps()
    end

  end
end
