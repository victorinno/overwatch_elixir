defmodule ConvidOverwatch.Repo.Migrations.PerguntaRespostaRelationship do
  use Ecto.Migration

  def change do
    alter table(:respostas) do
      add :pergunta_id, references(:perguntas)
    end
  end
end
