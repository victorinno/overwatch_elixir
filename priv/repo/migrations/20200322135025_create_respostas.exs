defmodule ConvidOverwatch.Repo.Migrations.CreateRespostas do
  use Ecto.Migration

  def change do
    create table(:respostas) do
      add :resposta, :string, size: 3000, null: false

      timestamps()
    end

  end
end
