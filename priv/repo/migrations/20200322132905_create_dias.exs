defmodule ConvidOverwatch.Repo.Migrations.CreateDias do
  use Ecto.Migration

  def change do
    create table(:dias) do
      add :dia, :date

      timestamps()
    end

  end
end
