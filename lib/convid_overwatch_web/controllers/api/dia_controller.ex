defmodule ConvidOverwatchWeb.Api.DiaController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Dia

  action_fallback ConvidOverwatchWeb.FallbackController

  def index(conn, _params) do
    dias = Overwatch.list_dias()
    render(conn, "index.json", dias: dias)
  end

  def create(conn, %{"dia" => dia_params}) do
    with {:ok, %Dia{} = dia} <- Overwatch.create_dia(dia_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_dia_path(conn, :show, dia))
      |> render("show.json", dia: dia)
    end
  end

  def show(conn, %{"id" => id}) do
    dia = Overwatch.get_dia!(id)
    render(conn, "show.json", dia: dia)
  end

  def update(conn, %{"id" => id, "dia" => dia_params}) do
    dia = Overwatch.get_dia!(id)

    with {:ok, %Dia{} = dia} <- Overwatch.update_dia(dia, dia_params) do
      render(conn, "show.json", dia: dia)
    end
  end

  def delete(conn, %{"id" => id}) do
    dia = Overwatch.get_dia!(id)

    with {:ok, %Dia{}} <- Overwatch.delete_dia(dia) do
      send_resp(conn, :no_content, "")
    end
  end
end
