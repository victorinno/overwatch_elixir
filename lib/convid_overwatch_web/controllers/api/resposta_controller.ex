defmodule ConvidOverwatchWeb.Api.RespostaController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Resposta

  action_fallback ConvidOverwatchWeb.FallbackController

  def index(conn, _params) do
    respostas = Overwatch.list_respostas()
    render(conn, "index.json", respostas: respostas)
  end

  def create(conn, %{"resposta" => resposta_params}) do
    with {:ok, %Resposta{} = resposta} <- Overwatch.create_resposta(resposta_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_resposta_path(conn, :show, resposta))
      |> render("show.json", resposta: resposta)
    end
  end

  def show(conn, %{"id" => id}) do
    resposta = Overwatch.get_resposta!(id)
    render(conn, "show.json", resposta: resposta)
  end

  def update(conn, %{"id" => id, "resposta" => resposta_params}) do
    resposta = Overwatch.get_resposta!(id)

    with {:ok, %Resposta{} = resposta} <- Overwatch.update_resposta(resposta, resposta_params) do
      render(conn, "show.json", resposta: resposta)
    end
  end

  def delete(conn, %{"id" => id}) do
    resposta = Overwatch.get_resposta!(id)

    with {:ok, %Resposta{}} <- Overwatch.delete_resposta(resposta) do
      send_resp(conn, :no_content, "")
    end
  end
end
