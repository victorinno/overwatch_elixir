defmodule ConvidOverwatchWeb.Api.PertenceController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Pertence

  action_fallback ConvidOverwatchWeb.FallbackController

  def index(conn, _params) do
    pertencimentos = Overwatch.list_pertencimentos()
    render(conn, "index.json", pertencimentos: pertencimentos)
  end

  def create(conn, %{"pertence" => pertence_params}) do
    with {:ok, %Pertence{} = pertence} <- Overwatch.create_pertence(pertence_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_pertence_path(conn, :show, pertence))
      |> render("show.json", pertence: pertence)
    end
  end

  def show(conn, %{"id" => id}) do
    pertence = Overwatch.get_pertence!(id)
    render(conn, "show.json", pertence: pertence)
  end

  def update(conn, %{"id" => id, "pertence" => pertence_params}) do
    pertence = Overwatch.get_pertence!(id)

    with {:ok, %Pertence{} = pertence} <- Overwatch.update_pertence(pertence, pertence_params) do
      render(conn, "show.json", pertence: pertence)
    end
  end

  def delete(conn, %{"id" => id}) do
    pertence = Overwatch.get_pertence!(id)

    with {:ok, %Pertence{}} <- Overwatch.delete_pertence(pertence) do
      send_resp(conn, :no_content, "")
    end
  end
end
