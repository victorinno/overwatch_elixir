defmodule ConvidOverwatchWeb.Api.PerguntaController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Pergunta

  action_fallback ConvidOverwatchWeb.FallbackController

  def index(conn, _params) do
    perguntas = Overwatch.list_perguntas()
    render(conn, "index.json", perguntas: perguntas)
  end

  def create(conn, %{"pergunta" => pergunta_params}) do
    with {:ok, %Pergunta{} = pergunta} <- Overwatch.create_pergunta(pergunta_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_pergunta_path(conn, :show, pergunta))
      |> render("show.json", pergunta: pergunta)
    end
  end

  def show(conn, %{"id" => id}) do
    pergunta = Overwatch.get_pergunta!(id)
    render(conn, "show.json", pergunta: pergunta)
  end

  def update(conn, %{"id" => id, "pergunta" => pergunta_params}) do
    pergunta = Overwatch.get_pergunta!(id)

    with {:ok, %Pergunta{} = pergunta} <- Overwatch.update_pergunta(pergunta, pergunta_params) do
      render(conn, "show.json", pergunta: pergunta)
    end
  end

  def delete(conn, %{"id" => id}) do
    pergunta = Overwatch.get_pergunta!(id)

    with {:ok, %Pergunta{}} <- Overwatch.delete_pergunta(pergunta) do
      send_resp(conn, :no_content, "")
    end
  end
end
