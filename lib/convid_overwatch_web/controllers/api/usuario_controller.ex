defmodule ConvidOverwatchWeb.Api.UsuarioController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Usuario

  action_fallback ConvidOverwatchWeb.FallbackController

  def index(conn, _params) do
    usuarios = Overwatch.list_usuarios()
    render(conn, "index.json", usuarios: usuarios)
  end

  def create(conn, %{"usuario" => usuario_params}) do
    with {:ok, %Usuario{} = usuario} <- Overwatch.create_usuario(usuario_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_usuario_path(conn, :show, usuario))
      |> render("show.json", usuario: usuario)
    end
  end

  def show(conn, %{"id" => id}) do
    usuario = Overwatch.get_usuario!(id)
    render(conn, "show.json", usuario: usuario)
  end

  def update(conn, %{"id" => id, "usuario" => usuario_params}) do
    usuario = Overwatch.get_usuario!(id)

    with {:ok, %Usuario{} = usuario} <- Overwatch.update_usuario(usuario, usuario_params) do
      render(conn, "show.json", usuario: usuario)
    end
  end

  def delete(conn, %{"id" => id}) do
    usuario = Overwatch.get_usuario!(id)

    with {:ok, %Usuario{}} <- Overwatch.delete_usuario(usuario) do
      send_resp(conn, :no_content, "")
    end
  end
end
