defmodule ConvidOverwatchWeb.RespostaController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Resposta

  def index(conn, _params) do
    respostas = Overwatch.list_respostas()
    render(conn, "index.html", respostas: respostas)
  end

  def new(conn, _params) do
    changeset = Overwatch.change_resposta(%Resposta{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"resposta" => resposta_params}) do
    case Overwatch.create_resposta(resposta_params) do
      {:ok, resposta} ->
        conn
        |> put_flash(:info, "Resposta created successfully.")
        |> redirect(to: Routes.resposta_path(conn, :show, resposta))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    resposta = Overwatch.get_resposta!(id)
    render(conn, "show.html", resposta: resposta)
  end

  def edit(conn, %{"id" => id}) do
    resposta = Overwatch.get_resposta!(id)
    changeset = Overwatch.change_resposta(resposta)
    render(conn, "edit.html", resposta: resposta, changeset: changeset)
  end

  def update(conn, %{"id" => id, "resposta" => resposta_params}) do
    resposta = Overwatch.get_resposta!(id)

    case Overwatch.update_resposta(resposta, resposta_params) do
      {:ok, resposta} ->
        conn
        |> put_flash(:info, "Resposta updated successfully.")
        |> redirect(to: Routes.resposta_path(conn, :show, resposta))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", resposta: resposta, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    resposta = Overwatch.get_resposta!(id)
    {:ok, _resposta} = Overwatch.delete_resposta(resposta)

    conn
    |> put_flash(:info, "Resposta deleted successfully.")
    |> redirect(to: Routes.resposta_path(conn, :index))
  end
end
