defmodule ConvidOverwatchWeb.PerguntaController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Pergunta

  def index(conn, _params) do
    perguntas = Overwatch.list_perguntas()
    render(conn, "index.html", perguntas: perguntas)
  end

  def new(conn, _params) do
    changeset = Overwatch.change_pergunta(%Pergunta{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"pergunta" => pergunta_params}) do
    case Overwatch.create_pergunta(pergunta_params) do
      {:ok, pergunta} ->
        conn
        |> put_flash(:info, "Pergunta created successfully.")
        |> redirect(to: Routes.pergunta_path(conn, :show, pergunta))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    pergunta = Overwatch.get_pergunta!(id)
    render(conn, "show.html", pergunta: pergunta)
  end

  def edit(conn, %{"id" => id}) do
    pergunta = Overwatch.get_pergunta!(id)
    changeset = Overwatch.change_pergunta(pergunta)
    render(conn, "edit.html", pergunta: pergunta, changeset: changeset)
  end

  def update(conn, %{"id" => id, "pergunta" => pergunta_params}) do
    pergunta = Overwatch.get_pergunta!(id)

    case Overwatch.update_pergunta(pergunta, pergunta_params) do
      {:ok, pergunta} ->
        conn
        |> put_flash(:info, "Pergunta updated successfully.")
        |> redirect(to: Routes.pergunta_path(conn, :show, pergunta))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", pergunta: pergunta, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    pergunta = Overwatch.get_pergunta!(id)
    {:ok, _pergunta} = Overwatch.delete_pergunta(pergunta)

    conn
    |> put_flash(:info, "Pergunta deleted successfully.")
    |> redirect(to: Routes.pergunta_path(conn, :index))
  end
end
