defmodule ConvidOverwatchWeb.PageController do
  use ConvidOverwatchWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
