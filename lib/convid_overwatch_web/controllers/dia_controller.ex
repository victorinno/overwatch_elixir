defmodule ConvidOverwatchWeb.DiaController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Dia

  def index(conn, _params) do
    dias = Overwatch.list_dias()
    render(conn, "index.html", dias: dias)
  end

  def new(conn, _params) do
    changeset = Overwatch.change_dia(%Dia{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"dia" => dia_params}) do
    case Overwatch.create_dia(dia_params) do
      {:ok, dia} ->
        conn
        |> put_flash(:info, "Dia created successfully.")
        |> redirect(to: Routes.dia_path(conn, :show, dia))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    dia = Overwatch.get_dia!(id)
    render(conn, "show.html", dia: dia)
  end

  def edit(conn, %{"id" => id}) do
    dia = Overwatch.get_dia!(id)
    changeset = Overwatch.change_dia(dia)
    render(conn, "edit.html", dia: dia, changeset: changeset)
  end

  def update(conn, %{"id" => id, "dia" => dia_params}) do
    dia = Overwatch.get_dia!(id)

    case Overwatch.update_dia(dia, dia_params) do
      {:ok, dia} ->
        conn
        |> put_flash(:info, "Dia updated successfully.")
        |> redirect(to: Routes.dia_path(conn, :show, dia))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", dia: dia, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    dia = Overwatch.get_dia!(id)
    {:ok, _dia} = Overwatch.delete_dia(dia)

    conn
    |> put_flash(:info, "Dia deleted successfully.")
    |> redirect(to: Routes.dia_path(conn, :index))
  end
end
