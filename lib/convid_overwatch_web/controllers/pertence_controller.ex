defmodule ConvidOverwatchWeb.PertenceController do
  use ConvidOverwatchWeb, :controller

  alias ConvidOverwatch.Overwatch
  alias ConvidOverwatch.Overwatch.Pertence

  def index(conn, _params) do
    pertencimentos = Overwatch.list_pertencimentos()
    render(conn, "index.html", pertencimentos: pertencimentos)
  end

  def new(conn, _params) do
    changeset = Overwatch.change_pertence(%Pertence{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"pertence" => pertence_params}) do
    case Overwatch.create_pertence(pertence_params) do
      {:ok, pertence} ->
        conn
        |> put_flash(:info, "Pertence created successfully.")
        |> redirect(to: Routes.pertence_path(conn, :show, pertence))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    pertence = Overwatch.get_pertence!(id)
    render(conn, "show.html", pertence: pertence)
  end

  def edit(conn, %{"id" => id}) do
    pertence = Overwatch.get_pertence!(id)
    changeset = Overwatch.change_pertence(pertence)
    render(conn, "edit.html", pertence: pertence, changeset: changeset)
  end

  def update(conn, %{"id" => id, "pertence" => pertence_params}) do
    pertence = Overwatch.get_pertence!(id)

    case Overwatch.update_pertence(pertence, pertence_params) do
      {:ok, pertence} ->
        conn
        |> put_flash(:info, "Pertence updated successfully.")
        |> redirect(to: Routes.pertence_path(conn, :show, pertence))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", pertence: pertence, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    pertence = Overwatch.get_pertence!(id)
    {:ok, _pertence} = Overwatch.delete_pertence(pertence)

    conn
    |> put_flash(:info, "Pertence deleted successfully.")
    |> redirect(to: Routes.pertence_path(conn, :index))
  end
end
