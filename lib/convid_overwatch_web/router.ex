defmodule ConvidOverwatchWeb.Router do
  use ConvidOverwatchWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ConvidOverwatchWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/dias", DiaController
    resources "/perguntas", PerguntaController
    resources "/respostas", RespostaController
    resources "/usuarios", UsuarioController
    resources "/pertencimentos", PertenceController
  end

  scope "/api", ConvidOverwatchWeb.Api do
    pipe_through :api

    resources "/dias", DiaController
    resources "/perguntas", PerguntaController
    resources "/respostas", RespostaController
    resources "/usuarios", UsuarioController
    resources "/pertencimentos", PertenceController
  end

  # Other scopes may use custom stacks.
  # scope "/api", ConvidOverwatchWeb do
  #   pipe_through :api
  # end
end
