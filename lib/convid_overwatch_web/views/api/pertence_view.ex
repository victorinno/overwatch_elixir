defmodule ConvidOverwatchWeb.Api.PertenceView do
  use ConvidOverwatchWeb, :view
  alias ConvidOverwatchWeb.Api.PertenceView

  def render("index.json", %{pertencimentos: pertencimentos}) do
    %{data: render_many(pertencimentos, PertenceView, "pertence.json")}
  end

  def render("show.json", %{pertence: pertence}) do
    %{data: render_one(pertence, PertenceView, "pertence.json")}
  end

  def render("pertence.json", %{pertence: pertence}) do
    %{id: pertence.id}
  end
end
