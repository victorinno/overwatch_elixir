defmodule ConvidOverwatchWeb.Api.PerguntaView do
  use ConvidOverwatchWeb, :view
  alias ConvidOverwatchWeb.Api.PerguntaView

  def render("index.json", %{perguntas: perguntas}) do
    %{data: render_many(perguntas, PerguntaView, "pergunta.json")}
  end

  def render("show.json", %{pergunta: pergunta}) do
    %{data: render_one(pergunta, PerguntaView, "pergunta.json")}
  end

  def render("pergunta.json", %{pergunta: pergunta}) do
    %{id: pergunta.id,
      pergunta: pergunta.pergunta}
  end
end
