defmodule ConvidOverwatchWeb.Api.UsuarioView do
  use ConvidOverwatchWeb, :view
  alias ConvidOverwatchWeb.Api.UsuarioView

  def render("index.json", %{usuarios: usuarios}) do
    %{data: render_many(usuarios, UsuarioView, "usuario.json")}
  end

  def render("show.json", %{usuario: usuario}) do
    %{data: render_one(usuario, UsuarioView, "usuario.json")}
  end

  def render("usuario.json", %{usuario: usuario}) do
    %{id: usuario.id,
      nome: usuario.nome,
      email: usuario.email,
      senha_criptografada: usuario.senha_criptografada}
  end
end
