defmodule ConvidOverwatchWeb.Api.RespostaView do
  use ConvidOverwatchWeb, :view
  alias ConvidOverwatchWeb.Api.RespostaView

  def render("index.json", %{respostas: respostas}) do
    %{data: render_many(respostas, RespostaView, "resposta.json")}
  end

  def render("show.json", %{resposta: resposta}) do
    %{data: render_one(resposta, RespostaView, "resposta.json")}
  end

  def render("resposta.json", %{resposta: resposta}) do
    %{id: resposta.id,
      resposta: resposta.resposta}
  end
end
