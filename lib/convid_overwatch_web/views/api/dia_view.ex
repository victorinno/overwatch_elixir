defmodule ConvidOverwatchWeb.Api.DiaView do
  use ConvidOverwatchWeb, :view
  alias ConvidOverwatchWeb.Api.DiaView

  def render("index.json", %{dias: dias}) do
    %{data: render_many(dias, DiaView, "dia.json")}
  end

  def render("show.json", %{dia: dia}) do
    %{data: render_one(dia, DiaView, "dia.json")}
  end

  def render("dia.json", %{dia: dia}) do
    %{id: dia.id,
      dia: dia.dia}
  end
end
