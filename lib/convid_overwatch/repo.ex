defmodule ConvidOverwatch.Repo do
  use Ecto.Repo,
    otp_app: :convid_overwatch,
    adapter: Ecto.Adapters.MyXQL
end
