defmodule ConvidOverwatch.Overwatch do
  @moduledoc """
  The Overwatch context.
  """

  import Ecto.Query, warn: false
  alias ConvidOverwatch.Repo

  alias ConvidOverwatch.Overwatch.Dia

  @doc """
  Returns the list of dias.

  ## Examples

      iex> list_dias()
      [%Dia{}, ...]

  """
  def list_dias do
    Repo.all(Dia)
  end

  @doc """
  Gets a single dia.

  Raises `Ecto.NoResultsError` if the Dia does not exist.

  ## Examples

      iex> get_dia!(123)
      %Dia{}

      iex> get_dia!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dia!(id), do: Repo.get!(Dia, id)

  @doc """
  Creates a dia.

  ## Examples

      iex> create_dia(%{field: value})
      {:ok, %Dia{}}

      iex> create_dia(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_dia(attrs \\ %{}) do
    %Dia{}
    |> Dia.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a dia.

  ## Examples

      iex> update_dia(dia, %{field: new_value})
      {:ok, %Dia{}}

      iex> update_dia(dia, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_dia(%Dia{} = dia, attrs) do
    dia
    |> Dia.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a dia.

  ## Examples

      iex> delete_dia(dia)
      {:ok, %Dia{}}

      iex> delete_dia(dia)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dia(%Dia{} = dia) do
    Repo.delete(dia)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking dia changes.

  ## Examples

      iex> change_dia(dia)
      %Ecto.Changeset{source: %Dia{}}

  """
  def change_dia(%Dia{} = dia) do
    Dia.changeset(dia, %{})
  end

  alias ConvidOverwatch.Overwatch.Pergunta

  @doc """
  Returns the list of perguntas.

  ## Examples

      iex> list_perguntas()
      [%Pergunta{}, ...]

  """
  def list_perguntas do
    Repo.all(Pergunta)
  end

  @doc """
  Gets a single pergunta.

  Raises `Ecto.NoResultsError` if the Pergunta does not exist.

  ## Examples

      iex> get_pergunta!(123)
      %Pergunta{}

      iex> get_pergunta!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pergunta!(id), do: Repo.get!(Pergunta, id)

  @doc """
  Creates a pergunta.

  ## Examples

      iex> create_pergunta(%{field: value})
      {:ok, %Pergunta{}}

      iex> create_pergunta(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pergunta(attrs \\ %{}) do
    %Pergunta{}
    |> Pergunta.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pergunta.

  ## Examples

      iex> update_pergunta(pergunta, %{field: new_value})
      {:ok, %Pergunta{}}

      iex> update_pergunta(pergunta, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pergunta(%Pergunta{} = pergunta, attrs) do
    pergunta
    |> Pergunta.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a pergunta.

  ## Examples

      iex> delete_pergunta(pergunta)
      {:ok, %Pergunta{}}

      iex> delete_pergunta(pergunta)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pergunta(%Pergunta{} = pergunta) do
    Repo.delete(pergunta)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pergunta changes.

  ## Examples

      iex> change_pergunta(pergunta)
      %Ecto.Changeset{source: %Pergunta{}}

  """
  def change_pergunta(%Pergunta{} = pergunta) do
    Pergunta.changeset(pergunta, %{})
  end

  alias ConvidOverwatch.Overwatch.Resposta

  @doc """
  Returns the list of respostas.

  ## Examples

      iex> list_respostas()
      [%Resposta{}, ...]

  """
  def list_respostas do
    Repo.all(Resposta)
  end

  @doc """
  Gets a single resposta.

  Raises `Ecto.NoResultsError` if the Resposta does not exist.

  ## Examples

      iex> get_resposta!(123)
      %Resposta{}

      iex> get_resposta!(456)
      ** (Ecto.NoResultsError)

  """
  def get_resposta!(id), do: Repo.get!(Resposta, id)

  @doc """
  Creates a resposta.

  ## Examples

      iex> create_resposta(%{field: value})
      {:ok, %Resposta{}}

      iex> create_resposta(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_resposta(attrs \\ %{}) do
    %Resposta{}
    |> Resposta.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a resposta.

  ## Examples

      iex> update_resposta(resposta, %{field: new_value})
      {:ok, %Resposta{}}

      iex> update_resposta(resposta, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_resposta(%Resposta{} = resposta, attrs) do
    resposta
    |> Resposta.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a resposta.

  ## Examples

      iex> delete_resposta(resposta)
      {:ok, %Resposta{}}

      iex> delete_resposta(resposta)
      {:error, %Ecto.Changeset{}}

  """
  def delete_resposta(%Resposta{} = resposta) do
    Repo.delete(resposta)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking resposta changes.

  ## Examples

      iex> change_resposta(resposta)
      %Ecto.Changeset{source: %Resposta{}}

  """
  def change_resposta(%Resposta{} = resposta) do
    Resposta.changeset(resposta, %{})
  end

  alias ConvidOverwatch.Overwatch.Usuario

  @doc """
  Returns the list of usuarios.

  ## Examples

      iex> list_usuarios()
      [%Usuario{}, ...]

  """
  def list_usuarios do
    Repo.all(Usuario)
  end

  @doc """
  Gets a single usuario.

  Raises `Ecto.NoResultsError` if the Usuario does not exist.

  ## Examples

      iex> get_usuario!(123)
      %Usuario{}

      iex> get_usuario!(456)
      ** (Ecto.NoResultsError)

  """
  def get_usuario!(id), do: Repo.get!(Usuario, id)

  @doc """
  Creates a usuario.

  ## Examples

      iex> create_usuario(%{field: value})
      {:ok, %Usuario{}}

      iex> create_usuario(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_usuario(attrs \\ %{}) do
    %Usuario{}
    |> Usuario.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a usuario.

  ## Examples

      iex> update_usuario(usuario, %{field: new_value})
      {:ok, %Usuario{}}

      iex> update_usuario(usuario, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_usuario(%Usuario{} = usuario, attrs) do
    usuario
    |> Usuario.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a usuario.

  ## Examples

      iex> delete_usuario(usuario)
      {:ok, %Usuario{}}

      iex> delete_usuario(usuario)
      {:error, %Ecto.Changeset{}}

  """
  def delete_usuario(%Usuario{} = usuario) do
    Repo.delete(usuario)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking usuario changes.

  ## Examples

      iex> change_usuario(usuario)
      %Ecto.Changeset{source: %Usuario{}}

  """
  def change_usuario(%Usuario{} = usuario) do
    Usuario.changeset(usuario, %{})
  end

  alias ConvidOverwatch.Overwatch.Pertence

  @doc """
  Returns the list of pertencimentos.

  ## Examples

      iex> list_pertencimentos()
      [%Pertence{}, ...]

  """
  def list_pertencimentos do
    Repo.all(Pertence)
  end

  @doc """
  Gets a single pertence.

  Raises `Ecto.NoResultsError` if the Pertence does not exist.

  ## Examples

      iex> get_pertence!(123)
      %Pertence{}

      iex> get_pertence!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pertence!(id), do: Repo.get!(Pertence, id)

  @doc """
  Creates a pertence.

  ## Examples

      iex> create_pertence(%{field: value})
      {:ok, %Pertence{}}

      iex> create_pertence(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pertence(attrs \\ %{}) do
    %Pertence{}
    |> Pertence.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a pertence.

  ## Examples

      iex> update_pertence(pertence, %{field: new_value})
      {:ok, %Pertence{}}

      iex> update_pertence(pertence, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pertence(%Pertence{} = pertence, attrs) do
    pertence
    |> Pertence.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a pertence.

  ## Examples

      iex> delete_pertence(pertence)
      {:ok, %Pertence{}}

      iex> delete_pertence(pertence)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pertence(%Pertence{} = pertence) do
    Repo.delete(pertence)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pertence changes.

  ## Examples

      iex> change_pertence(pertence)
      %Ecto.Changeset{source: %Pertence{}}

  """
  def change_pertence(%Pertence{} = pertence) do
    Pertence.changeset(pertence, %{})
  end
end
