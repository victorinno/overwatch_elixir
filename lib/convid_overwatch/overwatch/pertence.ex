defmodule ConvidOverwatch.Overwatch.Pertence do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pertencimentos" do

    timestamps()
  end

  @doc false
  def changeset(pertence, attrs) do
    pertence
    |> cast(attrs, [])
    |> validate_required([])
  end
end
