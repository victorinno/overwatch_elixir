defmodule ConvidOverwatch.Overwatch.Usuario do
  use Ecto.Schema
  import Ecto.Changeset

  schema "usuarios" do
    field :email, :string
    field :nome, :string
    field :senha_criptografada, :string

    # VIRTUAL FIELDS
    field :senha, :string, virtual: true
    field :senha_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(usuario, attrs) do
    usuario
    |> cast(attrs, [:nome, :email, :senha_criptografada])
    |> validate_required([:nome, :email])
    |> validate_length(:senha, min: 6)
    |> validate_confirmation(:senha)
    |> validate_length(:email, min: 3)
    |> unique_constraint(:email)
    |> downcase_username
    |> encrypt_password
  end

  defp encrypt_password(changeset) do
    password = get_change(changeset, :senha)
    if password do
      encrypted_password = Encryption.hash_password(password)
      put_change(changeset, :senha_criptografada, encrypted_password)
    else
      changeset
    end
  end

  defp downcase_username(changeset) do
    update_change(changeset, :email, &String.downcase/1)
  end
end
