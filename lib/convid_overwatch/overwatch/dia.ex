defmodule ConvidOverwatch.Overwatch.Dia do
  use Ecto.Schema
  import Ecto.Changeset

  schema "dias" do
    field :dia, :date

    timestamps()
  end

  @doc false
  def changeset(dia, attrs) do
    dia
    |> cast(attrs, [:dia])
    |> validate_required([:dia])
  end
end
