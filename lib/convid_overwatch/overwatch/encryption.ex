defmodule ConvidOverwatch.Overwatch.Encryption do
  alias Comeonin.Bcrypt
  alias ConvidOverwatch.Overwatch.Usuario

  def hash_password(senha), do: Bcrypt.hashpwsalt(senha)

  def validate_password(%Usuario{} = email, senha), do: Bcrypt.check_pass(email, senha)
end
