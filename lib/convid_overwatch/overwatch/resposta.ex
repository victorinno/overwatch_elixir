defmodule ConvidOverwatch.Overwatch.Resposta do
  use Ecto.Schema
  import Ecto.Changeset

  schema "respostas" do
    field :resposta, :string
    belongs_to :pergunta, ConvidOverwatch.Overwatch.Resposta

    timestamps()
  end

  @doc false
  def changeset(resposta, attrs) do
    resposta
    |> cast(attrs, [:resposta])
    |> validate_required([:resposta])
    |> validate_length(:resposta, max: 3000)
  end
end
