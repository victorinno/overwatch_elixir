defmodule ConvidOverwatch.Overwatch.Pergunta do
  use Ecto.Schema
  import Ecto.Changeset

  schema "perguntas" do
    field :pergunta, :string

    timestamps()
  end

  @doc false
  def changeset(pergunta, attrs) do
    pergunta
    |> cast(attrs, [:pergunta])
    |> validate_required([:pergunta])
    |> validate_length(:pergunta, max: 3000)
  end
end
